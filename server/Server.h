#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <stack>
#include <vector>
#include <queue>
#include <exception>
#include <iostream>
#include <string>
#include <mutex>
#include <fstream>
#include "Helper.h"

#define TWO 2
#define FIVE 5
#define NOT_FOUND -1
#define SRCUSER_DEFFERENCE 23
#define AUTHOR 8

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);

	void serverUpdateMessage(SOCKET cs, std::string srcUser, std::string dstUser);
	void clientUpdateMessage(SOCKET cs, std::string srcUser);
	std::string padding(int num, int len);
	int searchUser(std::string username);
	std::string filePath(std::string userOne, std::string userTwo);
	void messageQueueHandler();

	std::stack<std::thread> tStack;
	std::vector<std::string> _users;
	std::queue<std::string> _messages;
	SOCKET _serverSocket;
	bool areThereMessages;
};

