#include "Server.h"
std::mutex userMtx;
std::mutex messageMtx;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 
	this->areThereMessages = false;
	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
		while (this->tStack.empty())
		{
			this->tStack.pop();
		}
		this->_users.~vector();
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	std::thread messagesThread(&Server::messageQueueHandler, this);
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	// the function that handle the conversation with the client
	this->tStack.push(std::thread(&Server::clientHandler, this, client_socket));
	this->tStack.top().detach();
}


void Server::clientHandler(SOCKET clientSocket)
{
	int userLen = 0;
	std::string currUser;
	try
	{
		while (true)
		{
			switch (Helper::getMessageTypeCode(clientSocket))
			{
			case MT_CLIENT_LOG_IN:
				userLen = Helper::getIntPartFromSocket(clientSocket, TWO);
				currUser = Helper::getStringPartFromSocket(clientSocket, userLen);
				userMtx.lock();
				this->_users.push_back(currUser);
				userMtx.unlock();
				serverUpdateMessage(clientSocket, currUser, "");
				break;
			case MT_CLIENT_UPDATE:
				clientUpdateMessage(clientSocket, currUser);
			}
		}
	}
	catch (const std::exception& e)//if client closed program
	{
		userMtx.lock();
		this->_users.erase(this->_users.begin() + searchUser(currUser)); //deleting user from users vector
		userMtx.unlock();
		closesocket(clientSocket);// Closing the socket (in the level of the TCP protocol)
	}
}

void Server::serverUpdateMessage(SOCKET cs, std::string srcUser, std::string dstUser)
{
	std::ifstream file;
	std::string line;
	std::string chatContent;
	std::string allUsers;
	std::string message;
	if (dstUser != "")
	{
		file.open(filePath(srcUser, dstUser));
		if (file)
		{
			while (getline(file, line))
			{
				chatContent += line;//saving files content
			}
		}
	}
	int i = 0;
	userMtx.lock();
	for (i = 0; i < this->_users.size(); i++)
	{
		allUsers += this->_users[i] + "&";
	}
	allUsers = allUsers.substr(0, allUsers.size() - 1);//removing last &
	userMtx.unlock();
	message = "101" + padding(chatContent.size(), FIVE) + chatContent + padding(dstUser.size(), TWO) + dstUser + padding(allUsers.size(), FIVE) + allUsers;
	send(cs, message.c_str(), message.size(), 0);
}

void Server::clientUpdateMessage(SOCKET cs, std::string srcUser)
{
	int userLen = Helper::getIntPartFromSocket(cs, TWO);
	std::string dstUser = Helper::getStringPartFromSocket(cs, userLen);
	int messageLen = Helper::getIntPartFromSocket(cs, FIVE);
	std::string message = Helper::getStringPartFromSocket(cs, messageLen);
	userMtx.lock();
	int doesUserExist = searchUser(dstUser);
	userMtx.unlock();
	if(doesUserExist == NOT_FOUND)
	{
		messageLen = 0;
		message = "";
	}
	else if(messageLen != 0)
	{
		messageMtx.lock();
		this->_messages.push(dstUser + "&MAGSH_MESSAGE&&Author&" + srcUser + "&DATA&" + message); //pushing message in formation to the queue
		this->areThereMessages = true;
		messageMtx.unlock();
	}
	serverUpdateMessage(cs, srcUser, dstUser);
}

std::string Server::padding(int num, int len)
{
	std::string numStr = std::to_string(num);
	while (numStr.length() < len)
	{
		numStr = "0" + numStr;
	}
	return numStr;
}

int Server::searchUser(std::string username)//entered when mutex is locked
{
	int i = 0;
	bool ans = false;
	for (i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] == username)
		{
			return i;
		}
	}
	return NOT_FOUND;
}

std::string Server::filePath(std::string userOne, std::string userTwo)
{
	std::string filePath;
	if (userOne.compare(userTwo) > 0)//username & dstname
	{
		filePath = userOne + "&" + userTwo + ".txt";
	}
	else //dstname & username
	{
		filePath = userTwo + "&" + userOne + ".txt";
	}
	return filePath;
}

void Server::messageQueueHandler()
{
	std::ofstream file;
	std::string currMessage;
	std::string dstUser;
	int dstSize = 0;
	std::string srcUser;
	int srcSize = 0;
	while (true)
	{
		if (this->areThereMessages)
		{
			messageMtx.lock();
			currMessage = this->_messages.front();
			this->_messages.pop();
			if (this->_messages.empty())
			{
				this->areThereMessages = false;
			}
			messageMtx.unlock();
			dstSize = currMessage.find("&");
			dstUser = currMessage.substr(0, dstSize);
			srcSize = currMessage.find("&DATA") - currMessage.find("&Author&") - AUTHOR;
			srcUser = currMessage.substr(dstSize + SRCUSER_DEFFERENCE, srcSize);
			userMtx.lock();
			file.open(filePath(srcUser, dstUser), std::ios_base::app);//append mode
			userMtx.unlock();
			file << currMessage.substr(currMessage.find("&"));
			file.close();
		}
	}
}