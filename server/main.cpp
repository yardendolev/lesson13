#pragma once
#include "Server.h"
#include "WSAInitializer.h"
#define PORT 8826
#pragma comment(lib, "Ws2_32.lib")

int main(void)
{
	WSAInitializer wsa;
	Server s;
	s.serve(PORT);

	s.~Server();
	wsa.~WSAInitializer();
	exit(0);
	return 0;
}

